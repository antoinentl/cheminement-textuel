# Cheminement textuel
Source du texte et de la présentation réalisés dans le cadre du cours de Jean-Simon Desrochers, La pratiques d'écriture, à l'Université de Montréal en décembre 2019.
La présentation `index.md` a été écrite pas à pas, chaque commit contenant un message qui constitue le texte d'accompagnement, parallèle.
Le résultat est visible en ligne : [quaternum.net/chemin](https://www.quaternum.net/chemin)

Voici la commande utilisée pour extraire les messages des commits :

```
git log --reverse --pretty=format:"%h %s" > log.txt
```

Inspiration : [Brendan Dawes, Using a Git Repo to create a physical document of the work](http://brendandawes.com/blog/gitbook).
