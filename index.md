# Cheminement textuel
## Versions et processus

<small>Antoine Fauchié<br>
La pratique d'écriture</small>

===n===

## Mise en garde
Laissons les brouillons tranquilles.

===n===

## Origines du questionnement
Croiser les pratiques d'écriture : le code (informatique) et le texte (littéraire).

===n===

## Le texte
Superposition, substitution et versions abandonnées.

===n===

## Le code
Déclaration, navigation et versions parallèles.

===n===

## Le flux du texte
Le cheminement textuel avec Git.

===n===

## Git ?
Git permet de versionner du texte.

===n===

## Git !
Un journal d'écriture, un carnet de doutes.

===n===

## L'écriture dans l'écriture
Commenter la pratique ?

===n===

## Sonder la pratique
La façon de faire et le résultat.

===n===

## Le fantasme du brouillon
Envisager l'inutilité du méta texte.

===n===

## Une démonstration par l'exemple
[quaternum.net/chemin](https://www.quaternum.net/chemin)
